import re

import github

g = github.Github('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')

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    with open('list.txt', 'r') as f:
        lines = f.readlines()
        data = {k.replace('\n', ''): 'Проект загружен не верно' for k in lines}

    for row in data:
        print(row)
        try:
            repo = g.get_repo(row.replace('.git', '').replace('https://github.com/', ''))
            try:
                branch = repo.get_branch('main')
            except github.GithubException:
                branch = repo.get_branch('master')
            tree = repo.get_git_tree(branch.raw_data['commit']['sha'], recursive=True)
            for node in tree.tree:
                if node.type == 'blob' and node.path.endswith('.sln'):
                    data[row] = [re.findall(r'\{([^\}]+)', item)[1] for item in
                                 repo.get_contents(node.path).decoded_content.decode().split('\n') if
                                 item.startswith('Project')]
                    print(data[row])
                if node.type == 'blob' and node.path.endswith('.csproj'):
                    print(node.path)
        except github.GithubException as ex:
            data[row] = ex.data['message']

    for ind1 in range(len(data)):
        key_list = list(data.keys())
        if not isinstance(data[key_list[ind1]], list):
            continue
        list1 = data[key_list[ind1]]
        for ind2 in range(ind1 + 1, len(data)):
            if not isinstance(data[key_list[ind2]], list):
                continue
            list2 = data[key_list[ind2]]
            for item1 in list1:
                for item2 in list2:
                    if item1 == item2:
                        data[key_list[ind1]] = f'ЗА УЧАСТИЕ В СПИСЫВАНИИ ВЫ ПОПАЛИ В ЧЕРНЫЙ СПИСОК С {key_list[ind2]}'
                        data[key_list[ind2]] = f'ЗА УЧАСТИЕ В СПИСЫВАНИИ ВЫ ПОПАЛИ В ЧЕРНЫЙ СПИСОК С {key_list[ind1]}'

    for key, val in data.items():
        if isinstance(val, list):
            print('Уникальная работа')
        else:
            print(val)
